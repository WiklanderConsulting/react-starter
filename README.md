# README #

### What is this repository for? ###

A starter application for new (P)react PWA projects

### How do I get set up? ###

1. Install preact-cli: `npm install --global preact-cli`
2. Install dependencies: `npm install`
3. Start the development server: `preact watch`
