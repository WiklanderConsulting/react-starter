import { h, Component } from 'preact';
import { Container } from 'semantic-ui-react'

export default class Home extends Component {
  render(props, state, context) {
    return (
      <Container>
        <h1>Home</h1>
        <p>This is the Home component.</p>
      </Container>
    );
  }
}
