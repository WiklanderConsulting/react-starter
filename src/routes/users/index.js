import { h, Component } from 'preact';
import { Container, Icon, Label, Menu, Table } from 'semantic-ui-react'

export default class Users extends Component {
  render(props, state, context) {
    return (
      <Container>
        <h1>Users</h1>

        <Table celled>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Name</Table.HeaderCell>
              <Table.HeaderCell>Email</Table.HeaderCell>
              <Table.HeaderCell>Status</Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            <Table.Row>
              <Table.Cell>Kalle</Table.Cell>
              <Table.Cell>kalle.karlsson@vr.se</Table.Cell>
              <Table.Cell><Label color="green">Active</Label></Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>Cell</Table.Cell>
              <Table.Cell>Cell</Table.Cell>
              <Table.Cell>Cell</Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>Cell</Table.Cell>
              <Table.Cell>Cell</Table.Cell>
              <Table.Cell>Cell</Table.Cell>
            </Table.Row>
          </Table.Body>
        </Table>
      </Container>
    );
  }
}
