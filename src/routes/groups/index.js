import { h, Component } from 'preact';
import { Container } from 'semantic-ui-react'

export default class Groups extends Component {
  render(props, state, context) {
    return (
      <Container>
        <h1>Groups</h1>
        <p>This is the Groups component.</p>
      </Container>
    );
  }
}
