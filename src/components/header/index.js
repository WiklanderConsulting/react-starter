import { h, Component } from 'preact';
import { Link } from 'preact-router/match';
import { Menu } from 'semantic-ui-react'

export default class Header extends Component {
  render() {
    return (
      <Menu inverted color={"blue"}>
        <Menu.Item header>RUT Admin</Menu.Item>
        <Menu.Item as={Link} href="/users"       activeClassName="active">Users</Menu.Item>
        <Menu.Item as={Link} href="/groups"      activeClassName="active">Groups</Menu.Item>
        <Menu.Item as={Link} href="/roles"       activeClassName="active">Roles</Menu.Item>
        <Menu.Item as={Link} href="/permissions" activeClassName="active">Permissions</Menu.Item>
      </Menu>
    )
  }
}
