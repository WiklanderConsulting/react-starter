import { h, Component } from 'preact';
import { Router }       from 'preact-router';

import Header           from './header';
import Home             from '../routes/home';
import Users            from '../routes/users';
import Groups           from '../routes/groups';
import Roles            from '../routes/roles';
import Permissions      from '../routes/permissions';
import Profile          from '../routes/profile';

import '../../node_modules/semantic-ui-css/semantic.css';

// import Home from 'async!./home';
// import Profile from 'async!./profile';

export default class App extends Component {
  /** Gets fired when the route changes.
   *  @param {Object} event       "change" event from [preact-router](http://git.io/preact-router)
   *  @param {string} event.url   The newly routed URL
   */
  handleRoute = event => {
    this.currentUrl = event.url;
  };

  render() {
    return (
      <div id="app">
        <Header />
        <Router onChange={this.handleRoute}>
          <Home         path="/" />
          <Users        path="/users"       />
          <Groups       path="/groups"      />
          <Roles        path="/roles"       />
          <Permissions  path="/permissions" />
          <Profile      path="/profile"     />
        </Router>
      </div>
    );
  }
}
